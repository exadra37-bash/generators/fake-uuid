# BASH FAKE UUID GENERATOR

A bash package to generate a fake Universal Unique Identifier also know as UUID.


## How to Install

Using the [Bash Package Manager](https://gitlab.com/exadra37-bash/package-manager) just go to the folder where you want it 
installed and type:

```
$ bpm require exadra37-bash/generators fake-uuid last-stable-release gitlab.com
```


## How to Use 

```bash
$ fake-uuid

---> Fake UUID Generator <---

8618036b-40a7-43ff-b92a-b7cd0b6daee1

```

#### Quiet option

```bash
$ fake-uuid --quiet
8618036b-40a7-43ff-b92a-b7cd0b6daee1
```
or

```bash
$ fake-uuid -q
8618036b-40a7-43ff-b92a-b7cd0b6daee1
```